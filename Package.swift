import PackageDescription

let package = Package(
    name: "Pulse",
    dependencies:[
        .Package(url: "https://gitlab.com/boterock/CPulse.git",
                 majorVersion: 1),
    ]
)
