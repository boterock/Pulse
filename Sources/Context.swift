import CPulse

public class Context {

    let c_context: OpaquePointer
    var c_server_info: String?

    var unsafeSelf: UnsafeMutableRawPointer! = nil

    var readyCallback: ()->() = {}
    var serverInfoCallback: (ServerInfo)->() = {_ in}
    // var sinkInfoCallback:(SinkInfo)->() = {_ in}

    public init(api: MainLoopAPI, clientName: String) {
        c_context = pa_context_new(api.c_mainloop_api, clientName)

        unsafeSelf = UnsafeMutableRawPointer(Unmanaged.passUnretained(self).toOpaque())
        
        pa_context_set_state_callback(c_context, {
            context, data in
            
            let this = Unmanaged<Context>.fromOpaque(data!).takeUnretainedValue()
            
            // print("state callback")
            let state = pa_context_get_state(context)
            
            this.stateCallback(state: state)
            
        }, unsafeSelf)
        
        
    }

    func stateCallback(state: pa_context_state) {
        switch state {
            case PA_CONTEXT_UNCONNECTED :
                print("unconnected")	
            case PA_CONTEXT_CONNECTING 	:
                print("connecting")
            case PA_CONTEXT_AUTHORIZING :
                print("authorizing")	
            case PA_CONTEXT_SETTING_NAME :
                print("setting_name")	
            case PA_CONTEXT_READY :	
                print("ready")
                readyCallback()
            case PA_CONTEXT_FAILED 	:
                print("failed`")
            case PA_CONTEXT_TERMINATED :
                print("context_term")
            default:
                print("pailísima")
            } 
    }
    
    public func connect(callback: @escaping ()->() = {} ) {
        readyCallback = callback
        pa_context_connect(self.c_context, nil, PA_CONTEXT_NOFLAGS, nil)
    }

    public func serverInfo(callback: @escaping (ServerInfo)->() = {_ in}){

        serverInfoCallback = callback

        pa_context_get_server_info(c_context, {
            context, server_info, data in

            let this = Unmanaged<Context>.fromOpaque(data!).takeUnretainedValue()
           
            // print("server_info", server_info!.pointee)
            // let c_server_info = server_info!.pointee
            let serverInfo = ServerInfo(server_info!)

            this.getServerInfoCallback(info:serverInfo)
        }, unsafeSelf)
    }
    func getServerInfoCallback(info serverInfo: ServerInfo){
        serverInfoCallback(serverInfo)
    }

    // func sinkInfo(_ name:String, callback: @escaping (SinkInfo)-> () = {_ in}){
    //     sinkInfoCallback = callback
    //     pa_context_get_sink_info_by_name(c_context, name, {
    //         context, sink_info, eol, data in

    //         let this = Unmanaged<Context>.fromOpaque(data!).takeUnretainedValue()
           
    //         if eol != 0 {
    //             return
    //         }

    //         let si = sink_info!.pointee
    //         print(si)
            
    //         // volume = si.volume
            
    //     }, unsafeSelf)
    // }

    // func getSinkInfoCallback(info sinkInfo: SinkInfo){

    // }
}