import CPulse

public struct ServerInfo {

    public var userName: String = ""
    public var hostName: String = ""
    public var serverName: String = ""

    public var defaultSinkName: String = ""
    public var defaultSourceName: String = ""
    
}

extension ServerInfo {
    init(_ server_info: UnsafePointer<pa_server_info>) {

        let serverInfo = server_info.pointee

        userName = String(cString: serverInfo.user_name)
        hostName = String(cString: serverInfo.host_name)
        serverName = String(cString: serverInfo.server_name)
        defaultSinkName = String(cString: serverInfo.default_sink_name)
        defaultSourceName = String(cString: serverInfo.default_source_name)

    }
}